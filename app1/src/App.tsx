import { Box } from "@chakra-ui/react";
import React, { useState } from "react";
import CounterAppOne from "./components/CounterAppOne";
import "./index.css";
import { Provider } from 'react-redux';
// import sharedStore from 'container/sharedStore';
import Counter from "./components/Counter";
import ErrorBoundaryComponent from "./components/ErrorBoundaryComponent";
// import sharedStore from 'container/sharedStore';

const App = () => {
	// const [globalStore, setGlobalStore] = useState(sharedStore.getState())
	// console.log(sharedStore.getState());

	return (
		// <Provider store={globalStore}>
		<Box margin="1.2rem">
			<Box>APP-1</Box>
			<Box>
				<CounterAppOne name="123123123123123" />
			</Box>
			<Box>
				<ErrorBoundaryComponent>
					<Counter initialValue={25} />
				</ErrorBoundaryComponent>
			</Box>
		</Box>
		// </Provider>
	)
};

export default App;
