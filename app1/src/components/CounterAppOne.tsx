import React from 'react';

interface Props {
  name: string;
}

const CounterAppOne: React.FC<any> = ({ name }: Props) => {
  return (
    <div>{name}</div>
  );
};

export default CounterAppOne;