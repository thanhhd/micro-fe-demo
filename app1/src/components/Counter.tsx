import React, { useEffect, useState } from 'react';
import sharedStore from 'container/sharedStore';
import { GlobalStore } from 'redux-micro-frontend';
import { IncrementLocalCounter } from '../store/local.actions';
import { IncrementGlobalCounter } from '../store/global.actions';
import { CounterReducer } from '../store/counterReducer';
import { Button } from '@chakra-ui/react';

// Use sharedStore as needed

interface CounterProps {
    initialValue: number ;
}

const Counter: React.FC<CounterProps> = ({ initialValue = 0}) => {
    
    const [local, setLocal] = useState(0);
    const [global, setGlobal] = useState(0);

    console.log("counter render");

    const globalStore = GlobalStore.Get(false);
    const store = globalStore.CreateStore("CounterApp", CounterReducer, []);
    globalStore.RegisterGlobalActions("CounterApp", ["INCREMENT_GLOBAL"]);

    useEffect(() => {
        globalStore.SubscribeToGlobalState("CounterApp", (globalState) => {
            setLocal(globalState.CounterApp.local);
            setGlobal(globalState.CounterApp.global);
        });
    }, []);

    const incrementLocalCounter = () => {
        globalStore.DispatchAction("CounterApp", IncrementLocalCounter());
    }

    const incrementGlobalCounter = () => {
        globalStore.DispatchAction("CounterApp", IncrementGlobalCounter());
    }

    return (
        <div>
            <p>Count local: {local}</p>
            <Button onClick={incrementLocalCounter}>Increment Local</Button>
            <p>Count global: {global}</p>
            <Button onClick={incrementGlobalCounter}>Increment Global</Button>
        </div>
    );
};

export default Counter;