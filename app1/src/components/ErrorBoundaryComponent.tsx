import React from "react";

export default class ErrorBoundaryComponent extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error: any) {
    return { hasError: true };
  }

  componentDidCatch() { }

  render() {
    if (this.state.hasError === true) {
      return <h1>Something went wrong.</h1>;
    }

    return this.props.children;
  }
}