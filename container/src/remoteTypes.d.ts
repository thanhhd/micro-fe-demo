///<reference types="react" />

declare module "app1/CounterAppOne" {
	const CounterAppOne: any;
	export default CounterAppOne;
}
declare module "app2/CounterAppTwo" {
	const CounterAppTwo : any;
	export default CounterAppTwo;
}
declare module "app1/Counter" {
	const Counter;

	export default Counter;
}
