import React, { Suspense } from "react";
import { Routes, Route, createBrowserRouter, RouterProvider } from "react-router-dom";
import { ContainerApp } from "./components/ContainerApp";
// import ErrorBoundaryComponent from "./components/ErrorBoundaryComponent";
import "./index.css";

//  import CounterAppOne from "app1/CounterAppOne";
//  import CounterAppTwo from "app2/CounterAppTwo";
const CounterAppOne = React.lazy(() => import("app1/CounterAppOne"));
const CounterAppTwo = React.lazy(() => import("app2/CounterAppTwo"));
const Counter = React.lazy(() => import("app1/Counter"));

interface Props {
  name: string;
}

const App = () => {
  return (
    <>
      <Routes>

        <Route
          path="/"
          element={
            <ContainerApp
              CounterAppOne={CounterAppOne}
              CounterAppTwo={CounterAppTwo}
              Counter={Counter}
            />
          }
        />
          {/* <ErrorBoundaryComponent>
          </ErrorBoundaryComponent> */}
        <Route path="app1" element={
            <CounterAppOne name="name"/>
          }
        />
        <Route path="app2" element={
          // <ErrorBoundaryComponent>
            <CounterAppTwo/>
          // </ErrorBoundaryComponent>
        } />
        <Route path="app3" element={
          // <ErrorBoundaryComponent>
            <Counter />
          // </ErrorBoundaryComponent>
        } />
      </Routes>
    </>
  )
};

export default App;
