import { initialState } from "./sharedStore";

// reducers.js
const rootReducer = (state = initialState, action: any) => {
    switch (action.type) {
      case 'INCREMENT':
        return {
          ...state,
          counter: state.counter + 1,
        };
      // Handle other actions here
      default:
        return state;
    }
  };
  
  export default rootReducer;
  