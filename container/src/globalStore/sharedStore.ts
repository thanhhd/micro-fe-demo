import { createStore } from 'redux';
// sharedStore.js
import rootReducer from './reducers';

export const initialState = {
  // Define your initial shared state here
  counter: 0,
  // ...
};

const store = createStore(rootReducer, initialState);

export default store;
