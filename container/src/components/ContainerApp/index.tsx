import {
  Center,
  Box,
  Flex,
  Heading,
  Spinner,
  Button,
  Text,
  Image,
  Link,
} from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { Link as RouterLink } from "react-router-dom";
import { GlobalStore } from "redux-micro-frontend";
import ErrorBoundaryComponent from "../ErrorBoundaryComponent";

interface Props {
  name: string;
}

type ContainerAppProps = {
  CounterAppOne: any;
  CounterAppTwo: React.LazyExoticComponent<React.ComponentType<{}>>;
  Counter: any;
};

export const ContainerApp = ({
  CounterAppOne,
  CounterAppTwo,
  Counter,
}: ContainerAppProps) => {
  const [globalCounter, setGlobalCounter] = useState(0);
  const globalStore = GlobalStore.Get();
  const initialStore = globalStore.GetPartnerState("CounterApp")
  useEffect(() => {
    setGlobalCounter(initialStore?.global)
  }, [initialStore])
  
  
  console.log("containter render");
  useEffect(() => {
    console.log('====================================');
    console.log(123);
    console.log('====================================');
    const counterChanged = (counterState: any) => {
      setGlobalCounter(counterState.global);
    };

    try {
      globalStore.SubscribeToPartnerState(
        "App2",
        "CounterApp",
        counterChanged
      );
    } catch (error) {}

    // check support hooks
    // return () => {
    //   globalStore.UnsubscribeFromPartnerState(
    //     "App2",
    //     "CounterApp",
    //     counterChanged
    //   );
    // };
  });
  return (
    <Center
      height="100vh"
      width="100%"
      backgroundColor="#1B1A29"
      margin="0"
      p="0"
      flexDirection="column"
    >
      <Flex
        border="1px solid #151421"
        borderRadius="1rem"
        height="50vh"
        justifyContent="space-around"
        alignItems="center"
        flexDirection="column"
        padding="10rem"
        backgroundColor="#6e9"
      >
        <Heading color="#fff" paddingBottom="2rem">CONTAINER: {globalCounter}</Heading>
        <Flex direction="row" justifyContent="space-around">
          <React.Suspense fallback={<Spinner size="xl" />}>
            <Box
              p="2rem"
              mr="2rem"
              border="1px solid #aeaeae"
              borderRadius="1rem"
              backgroundColor="#fff"
            >
              <Heading color="#6e9" mb="1rem">
                Testing injected component
              </Heading>
              <ErrorBoundaryComponent>
                <CounterAppOne name="Imported component, this is some props"/>
              </ErrorBoundaryComponent>
              <Button mt="1rem" w="100%" to="/app1" as={RouterLink}>
                To M-App 1
              </Button>
            </Box>
            <Box
              p="2rem"
              mr="2rem"
              border="1px solid #aeaeae"
              borderRadius="1rem"
              backgroundColor="#fff"
            >
              <Heading color="#6e9" mb="1rem">
                M-Counter app
              </Heading>
              <ErrorBoundaryComponent>
                <Counter initialValue={5}/>
              </ErrorBoundaryComponent>
              <Button mt="1rem" w="100%" to="/app3" as={RouterLink}>
               Link to M-Counter app
              </Button>
            </Box>
          </React.Suspense>
          <React.Suspense fallback={<Spinner size="xl" />}>
            <Box
              p="2rem"
              border="1px solid #aeaeae"
              borderRadius="1rem"
              backgroundColor="#fff"
            >
              <Heading color="#6e9" mb="1rem">
                M-App 2
              </Heading>
              <CounterAppTwo />
              <Button
                mt="1rem"
                w="100%"
                alignSelf="center"
                to="/app2"
                as={RouterLink}
              >
                Link to M-App 2
              </Button>
            </Box>
          </React.Suspense>
        </Flex>
      </Flex>
    </Center>
  );
};
