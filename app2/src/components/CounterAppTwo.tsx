import React, { useEffect, useState } from 'react';
import { GlobalStore } from 'redux-micro-frontend';

const Counter = () => {
  const [globalCounter, setGlobalCounter] = useState(0);
  const globalStore = GlobalStore.Get();
  const initialStore = globalStore.GetPartnerState("CounterApp")
  console.log("app2 render");
  

  // useEffect(() => {
  //   setGlobalCounter(initialStore?.global)
  // }, [initialStore])

  useEffect(() => {
    const counterChanged = (counterState: any) => {
      setGlobalCounter(counterState.global);
    };

    try {
      globalStore.SubscribeToPartnerState(
        "App2",
        "CounterApp",
        counterChanged
      );
    } catch (error) {}
    setGlobalCounter(initialStore?.global)

    // check support hooks
    // return () => {
    //   globalStore.UnsubscribeFromPartnerState(
    //     "App2",
    //     "CounterApp",
    //     counterChanged
    //   );
    // };
  });

  return (
    <div>
      Global Counter: {globalCounter}
    </div>
  );
};

export default Counter;
